FROM python:3.8
MAINTAINER David Blaisonneau <david.blaisonneau@orange.com>

RUN apt update && apt install -y inkscape &&\
  pip install --upgrade pip &&\
  pip install sphinx sphinx_rtd_theme && \
  rm -rf /var/lib/apt/lists/* /tmp/* &&\
  rm -rf ~/.cache/pip

WORKDIR /doc
CMD ["sphinx-build -b html . public", "'.'"]
